package mx.isban.fe.bitacora.config;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="mxfe-ms-bitacora")
@Validated
public class MxfeMsBitacoraConfig {

	@NotEmpty
	private String nombreConfig;
	
	@NotEmpty
	private String urlPistas;

	/**
	 * @return La descripcion de la configuracion
	 */
	public String getNombreConfig() {
		return nombreConfig;
	}

	/**
	 * @param nombreConfig La descripcion de la configuracion
	 */
	public void setNombreConfig(String nombreConfig) {
		this.nombreConfig = nombreConfig;
	}

	public String getUrlPistas() {
		return urlPistas;
	}

	public void setUrlPistas(String urlPistas) {
		this.urlPistas = urlPistas;
	}
	

}