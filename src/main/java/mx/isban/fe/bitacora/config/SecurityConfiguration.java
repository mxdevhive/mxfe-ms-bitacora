package mx.isban.fe.bitacora.config;

import java.util.EnumMap;

import mx.isban.bboo.commons.security.RequestInterceptor;
import mx.isban.bboo.commons.security.SecurityServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityConfiguration {


	@SuppressWarnings("unused")
	private SecurityConfigProperties securityConfigProperties;
	
	/**
	 * Constructor
	 * @param securityConfigProperties Objeto de tipo SecurityConfigProperties
	 */
	@Autowired
	public SecurityConfiguration(SecurityConfigProperties securityConfigProperties) {
		this.securityConfigProperties = securityConfigProperties;
	}
	/**
	 * Metodo que crea un bean para mapear en los archivos de configuracion
	 * @param securityConfigProperties
	 * @return RequestInterceptor
	 */
	@Bean
	public RequestInterceptor requestInterceptor(SecurityConfigProperties securityConfigProperties) {
		
		EnumMap<SecurityServices,String> uris = new EnumMap<>(SecurityServices.class);
		uris.put(SecurityServices.URI_CONSULTAR_SESION, securityConfigProperties.getUriConsultarSesion());
		uris.put(SecurityServices.URI_ACTUALIZAR_SESION, securityConfigProperties.getUriActualizarSesion());
		uris.put(SecurityServices.URI_REGENERAR_COOKIE, securityConfigProperties.getUriRegenerarCookie());
		uris.put(SecurityServices.URI_VALIDAR_URI, securityConfigProperties.getUriValidarUri());
		return new RequestInterceptor(uris);
	}
}
