package mx.isban.fe.bitacora.config;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "app.security")
@Validated
public class SecurityConfigProperties {
	
	@NotEmpty
	@NotBlank
	@Length(min = 1)
	private String uriConsultarSesion;

	@NotEmpty
	@NotBlank
	@Length(min = 1)
	private String uriActualizarSesion;

	@NotEmpty
	@NotBlank
	@Length(min = 1)
	private String uriRegenerarCookie;

	@NotEmpty
	@NotBlank
	@Length(min = 1)
	private String uriValidarUri;

	@NotNull
	private Boolean securityInterceptorEnable;

	/**
	 * @return the uriConsultarSesion
	 */
	public String getUriConsultarSesion() {
		return uriConsultarSesion;
	}

	/**
	 * @param uriConsultarSesion
	 *            the uriConsultarSesion to set
	 */
	public void setUriConsultarSesion(String uriConsultarSesion) {
		this.uriConsultarSesion = uriConsultarSesion;
	}

	/**
	 * @return the uriActualizarSesion
	 */
	public String getUriActualizarSesion() {
		return uriActualizarSesion;
	}

	/**
	 * @param uriActualizarSesion
	 *            the uriActualizarSesion to set
	 */
	public void setUriActualizarSesion(String uriActualizarSesion) {
		this.uriActualizarSesion = uriActualizarSesion;
	}

	/**
	 * @return the uriRegenerarCookie
	 */
	public String getUriRegenerarCookie() {
		return uriRegenerarCookie;
	}

	/**
	 * @param uriRegenerarCookie
	 *            the uriRegenerarCookie to set
	 */
	public void setUriRegenerarCookie(String uriRegenerarCookie) {
		this.uriRegenerarCookie = uriRegenerarCookie;
	}

	/**
	 * @return the uriValidarUri
	 */
	public String getUriValidarUri() {
		return uriValidarUri;
	}

	/**
	 * @param uriValidarUri
	 *            the uriValidarUri to set
	 */
	public void setUriValidarUri(String uriValidarUri) {
		this.uriValidarUri = uriValidarUri;
	}

	/**
	 * @return the securityInterceptorEnable
	 */
	public Boolean isSecurityInterceptorEnable() {
		return securityInterceptorEnable;
	}

	/**
	 * @param securityInterceptorEnable
	 *            the securityInterceptorEnable to set
	 */
	public void setSecurityInterceptorEnable(Boolean securityInterceptorEnable) {
		this.securityInterceptorEnable = securityInterceptorEnable;
	}

}
