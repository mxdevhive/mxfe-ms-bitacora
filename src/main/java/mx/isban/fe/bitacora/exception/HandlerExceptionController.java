/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * 
 * Creado el 20/11/2018 10:40:45 (dd/mm/aaaa hh:mm)
 * 
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 */
package mx.isban.fe.bitacora.exception;

//import java.net.ConnectException;

//import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import mx.isban.bboo.commons.model.ResponseCode;
import mx.isban.bboo.commons.model.SingleResponse;

/**
 * 
 * @Proyecto: mxfe-ms-bitacora
 * @Paquete: mx.isban.fe.bitacora.exception
 * @Clase: HandlerExceptionController.java. <br/>
 * 
 * @version 1.0.0
 */
@ControllerAdvice
public class HandlerExceptionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HandlerExceptionController.class);

	

	/**
	 * Metodo que se encarga de validar las excepciones genericas
	 * 
	 * @param ex excepcion que se produce sin ser manejada
	 * @return Response con respuesta estandar http
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<SingleResponse<String>> handleAllException(Exception ex) {
		LOGGER.error("Ha ocurrido un error inesperado. Exception {} {}", ex.getMessage(), ex);
		LOGGER.info("Tipo --> " + ex.getClass().getName());
		SingleResponse<String> response = new SingleResponse<>();
		response.error(new ResponseCode("BOSG603", "Error general", "ERROR", "Ha ocurrido un error no controlado", ""),
				ex);
		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Metodo que se encarga de validar las excepciones genericas
	 * 
	 * @param ex excepcion que se produce sin ser manejada
	 * @return Response con respuesta estandar http
	 */
//    @ExceptionHandler({ PersistenceException.class, CannotCreateTransactionException.class, ConnectException.class })
//    public ResponseEntity<SingleResponse<String>> handleAllException(javax.persistence.PersistenceException ex) {
//        LOGGER.error("Ha ocurrido un error inesperado. Exception {} {}", ex.getMessage(), ex);
//        SingleResponse<String> response = new SingleResponse<>();
//        response.error(codes.getResponseCode("BOSG608"), ex);
//        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//    }

}
