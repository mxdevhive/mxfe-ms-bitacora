package mx.isban.fe.bitacora.controller;

import io.swagger.annotations.ApiOperation;
import mx.isban.bboo.commons.exception.BusinessException;
import mx.isban.fe.autenticacion.service.IAutenticacionService;
import mx.isban.fe.bitacora.dto.AutenticationRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller para el registro de pistas de auditoria
 * 
 */
@RestController
@RequestMapping("/pistasformatos")
@CrossOrigin(maxAge = 3600)
public class PistasController {

	/** Logger */
	private static final Logger LOGGER = LogManager.getLogger(PistasController.class);

	/** Inyección de capa service */
	@Autowired
	private IAutenticacionService autenticacionService;

	/**
	 * End-Point para el registro de pistas de auditoria
	 * 
	 * @param request Clase que tiene datos para el registro de pistas
	 * @return void
	 */
	@ApiOperation(value = "End-Point para registro de pistas de auditoria de la OSI")
	@PostMapping(path = "/pistas")
	public void getPartie(@RequestBody AutenticationRequest request) {
		try {
			LOGGER.error(request);
			/**
			 * Se realiza invocación al registro de pistas de audotoria, no se espera
			 * respuesta
			 */
			autenticacionService.registarPistaAuditoria(request);
		} catch (BusinessException e) {
			LOGGER.error("Error al realizar el registro de la pista de auditoria {} {}", e);
		}
	}

}
