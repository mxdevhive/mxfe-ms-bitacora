/**
 * Neoris
 * Todos los derechos reservados
 * <p>
 * SecurityConfiguredAdapter.java
 * <p>Clase para que registra el interceptor y verifica si 
 * SecurityInterceptor esta deshabilitado o habilitado.</p>
 * <p>
 * Control de versiones:
 * <p>
 * Version  Date/Hour               By                  Company     Description
 * -------  -------------------     ----------------    --------    -----------------------------------------------------------------
 * 1.0      02/02/2018 HH:MM:SS     DCS                 Neoris       Creación de clase
 */
package mx.isban.fe.bitacora.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import mx.isban.bboo.commons.security.RequestInterceptor;
import mx.isban.fe.bitacora.config.SecurityConfigProperties;

/**
 * <b>Clase SecurityConfiguredAdapter extiende de WebMvcConfigurerAdapter</b>
 * <br>
 * <p>
 * Clase para que registra el interceptor y verifica si SecurityInterceptor esta
 * deshabilitado o habilitado.
 * </p>
 * 
 * @author juan.perezr
 * @version 1.0
 * @since 2018
 */
@Configuration
public class CustomWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomWebMvcConfigurerAdapter.class);

	private SecurityConfigProperties securityConfigProperties;
	private RequestInterceptor requestInterceptor;

	/**
     * Constructor que recibe como parametros los siguientes objetos:
     * @param securityConfigProperties
     * @param requestInterceptor
     * @param ieCacheInterceptorAdapter
     */
	@Autowired
	public CustomWebMvcConfigurerAdapter(
			SecurityConfigProperties securityConfigProperties,
			RequestInterceptor requestInterceptor) {
		this.securityConfigProperties = securityConfigProperties;
		this.requestInterceptor = requestInterceptor;
	}

	/**
	 * Registro de los interceptores para el microservicio
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		LOGGER.info("Registrando Interceptor de seguridad");

		if (securityConfigProperties.isSecurityInterceptorEnable()) {
//			registry.addInterceptor(requestInterceptor);
		} else {
			LOGGER.warn("SecurityInterceptor deshabilitado.");
		}

	}

}
