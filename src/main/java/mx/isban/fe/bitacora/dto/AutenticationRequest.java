package mx.isban.fe.bitacora.dto;

public class AutenticationRequest {
	
	private String code;
	private String user; //ID Usuario/Código de cliente
	private String remoteAddr; //Dirección IP/Terminal
	private String sessionId; //Id de Sesión
	private String statusOper; //Estatus de la operación
	private String reference; //Referencia
	private String fileName; //Nombre del Archivo
	private String webInstance; //Nombre del Archivo
	private String webService; //servicio web
	private String msgResponse; //servicio web
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRemoteAddr() {
		return remoteAddr;
	}
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getStatusOper() {
		return statusOper;
	}
	public void setStatusOper(String statusOper) {
		this.statusOper = statusOper;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getWebInstance() {
		return webInstance;
	}
	public void setWebInstance(String webInstance) {
		this.webInstance = webInstance;
	}
	public String getWebService() {
		return webService;
	}
	public void setWebService(String webService) {
		this.webService = webService;
	}
	public String getMsgResponse() {
		return msgResponse;
	}
	public void setMsgResponse(String msgResponse) {
		this.msgResponse = msgResponse;
	}
	@Override
	public String toString() {
		return "AutenticationRequest [code=" + code + ", user=" + user
				+ ", remoteAddr=" + remoteAddr + ", sessionId=" + sessionId
				+ ", statusOper=" + statusOper + ", reference=" + reference
				+ ", fileName=" + fileName + ", webInstance=" + webInstance
				+ ", webService=" + webService + ", msgResponse=" + msgResponse + "]";
	}
	
}
