package mx.isban.fe.bitacora.enums;

public enum Transacciones {
	
	DEFAULT(							"OPERACION_DESCONOCIDA",				"Operacion desconocida",					"/",												"Operacion desconocida","" ,"FEI_000"),
	OBTIENE_BITACORA(					"OBTIENE_BITACORA",						"Obtener Bitacora",							"/eformats/bitacoras",								"Obtiene la información de la bitacora de algún formato por el id de bitacora",		"CONSULTA A BASE DE DATOS" ,"FEI_001"),
	GUARDA_BITACORA(					"GUARDA_BITACORA",						"Guardar Bitacora",							"/eformats/bitacoras",								"Guarda en la bitácora el uso de un formato",										"REGISTRO EN BASE DE DATOS" ,"FEI_002"),
	CONSULTA_CATALOGO_FMT(				"CONSULTA_CATALOGO_FMT",				"Obtener Catálogo de Formatos",				"/eformats/catFormatos/{}",							"Obtiene una lista de formatos por una palabra clave de búsqueda",					"CONSULTA A BASE DE DATOS" ,"FEI_003"),
	CONSULTA_CATALOGO(					"CONSULTA_CATALOGO",					"Obtener Catalogo",							"/eformats/catalogos",								"Obtiene los datos de un catálogo por su clave de catálogo",						"CONSULTA A BASE DE DATOS" ,"FEI_004"),
	CONSULTA_FACULTAD_POR_NUMERO(		"CONSULTA_FACULTAD_POR_NUMERO",			"Obtener Facultades por Num de Facultad",	"/eformats/facultades/{numFactPath}",				"Obtiene una lista de facultades por su número de facultad",						"CONSULTA A BASE DE DATOS" ,"FEI_005"),
	CONSULTA_FACULTAD_POR_PERFIL(		"CONSULTA_FACULTAD_POR_PERFIL",			"Obtener Facultades por Id de Perfil",		"/eformats/facultades/perfil/{idPerfil}",			"Obtiene una lista de facultades por su id de perfil",								"CONSULTA A BASE DE DATOS" ,"FEI_006"),
	CONSULTA_FACULTAD_EXTRAORDINARIA(	"CONSULTA_FACULTAD_EXTRAORDINARIA",		"Obtener Facultades Extraordinarias",		"/eformats/facultades/extraordinaria/{idTipoPerfil}","Obtiene una lista de facultades extraordinarias por el tipo de perfil asociado",	"CONSULTA A BASE DE DATOS" ,"FEI_007"),
	CONSULTA_FACULTAD_POR_TIPO_DE_CUENTA("CONSULTA_FACULTAD_POR_TIPO_DE_CUENTA","Obtener Facultades por Tipo de Cuenta",	"/eformats/facultades/byTipoCuenta/{tipoCuenta}",	"Obtiene una lista de facultades por el tipo de cuenta asociado",					"CONSULTA A BASE DE DATOS" ,"FEI_008"),
	CONSULTA_FORMATOS_FAVORITOS(		"CONSULTA_FORMATOS_FAVORITOS",			"Obtener Formatos Favoritos",				"/eformats/favoritos",								"Obtiene una lista de los formatos favoritos para un usuario en particular",		"CONSULTA A BASE DE DATOS" ,"FEI_009"),
	CONSULTA_FORMATOS_MAS_UTILIZADOS(	"CONSULTA_FORMATOS_MAS_UTILIZADOS",		"Obtener Formatos más Utilizados",			"/eformats/formatosMasUtilizados",					"Obtiene los formatos más utilizados por un usuario en particular",					"CONSULTA A BASE DE DATOS" ,"FEI_010"),
	CONSULTA_FORMATOS_1(				"CONSULTA_FORMATOS",					"Obtener Formatos",							"/eformats/formats",								"Obtiene la información del formato por su id",										"CONSULTA A BASE DE DATOS" ,"FEI_011"),
	CONSULTA_FORMATOS(					"CONSULTA_FORMATOS",					"Obtener Formatos",							"/eformats/formatos",								"Obtiene la información del formato por su id",										"CONSULTA A BASE DE DATOS" ,"FEI_012"),
	CONSULTA_ENTIDADES_FEDERATIVAS(		"CONSULTA_ENTIDADES_FED",				"Obtener Entidades Federativas",			"/eformats/catEntidades",							"Consulta catalogo de entidades federativas",										"CONSULTA A BASE DE DATOS" ,"FEI_013"),
	CONSULTA_MUNICIPIOS(				"CONSULTA_MUNICIPIOS",					"Obtener Municipios",						"/eformats/catEntidades/municipios",				"Consulta catalogo de municipios",													"CONSULTA A BASE DE DATOS" ,"FEI_014"),
	CONSULTA_PERFILES(					"CONSULTA_PERFILES",					"Obtener Perfiles",							"/eformats/perfiles",								"Obtiene la información de un perfil por su id de perfil",							"CONSULTA A BASE DE DATOS" ,"FEI_015"),
	CONSULTA_RESOLVER(					"CONSULTA_RESOLVER",					"Consultar Datos Resolver",					"/eformats/resolvers/consultaDatosResolverGet/",	"Consulta los datos de un usuario y su puesto o centro de costos",					"CONSULTA A BASE DE DATOS" ,"FEI_015"),
	CONSULTA_PUESTO(					"CONSULTA_PUESTO",						"Consultar Puesto",							"/eformats/resolvers/consultaPuestosGet/",			"Consulta los puestos de un usuario",												"CONSULTA A BASE DE DATOS" ,"FEI_016"),
	CONSULTA_CENTRO_COSTOS(				"CONSULTA_CENTRO_COSTOS",				"Consultar Centro de Costos",				"/eformats/resolvers/consultaCentroCostosGet/",		"Consulta los centros de costos",													"CONSULTA A BASE DE DATOS" ,"FEI_017"),
	INICIA_SESION(						"INICIA_SESION",						"Obtener/Iniciar Sesión",					"/eformats/sessions/getSession/",					"Obtiene el valor de la cookie aplicativa para el inicio de sesión",				"CONSULTA A BASE DE DATOS" ,"FEI_018"),
	CONSULTA_TIPOS_PERFILES(			"CONSULTA_TIPOS_PERFILES",				"Obtener Tipos de Perfiles",				"/eformats/tiposPerfiles",							"Obtiene los tipos de perfiles disponibles",										"CONSULTA A BASE DE DATOS" ,"FEI_019"),
	GENERA_REPORTE(						"GENERA_REPORTE",						"Generar Reporte",							"/reportes/rest/reporte",							"Genera un PDF que contiene el reporte generado con base en parámetros del mismo",	"GENERACION DEL PDF" ,"FEI_020"),
	CONSULTA_CLIENTE(					"CONSULTA_CLIENTE",						"Obtener Clientes",							"/transacciones/rest/clientes",						"Obtiene la información de un cliente",												"CONSULTA A TRX 390" ,"FEI_021"),
	CONSULTA_CUENTA(					"CONSULTA_CUENTA",						"Obtener Cuentas",							"/transacciones/rest/cuentas",						"Obtiene la información de una cuenta por su número de cuenta",						"CONSULTA A TRX 390" ,"FEI_022"),
	CONSULTA_TIPO_A3(					"CONSULTA_TIPO_A3",						"Obtener ODP1",								"/transacciones/rest/odp1",							"Obtiene la información del odp1",													"CONSULTA A TRX 390" ,"FEI_023"),
	CONSULTA_SUCURSAL(					"CONSULTA_SUCURSAL",					"Obtener Sucursales",						"/transacciones/rest/sucursales",					"Obtiene la información de las sucursales asociadas",								"CONSULTA A TRX 390" ,"FEI_024"),
	CONSULTA_CUENTAS_ASOCIADAS(			"CONSULTA_CUENTAS_ASOCIADAS",			"Obtener PE44",								"/transacciones/rest/cuentas_asociadas",			"Obtiene la información de el pe44",												"CONSULTA A TRX 390" ,"FEI_025"),
	CONSULTA_CUENTAS_POR_MOVIL(			"CONSULTA_CUENTAS_POR_MOVIL",			"Obtener Cuentas por Móvil",				"/transacciones/rest/cuentas_moviles",				"Obtiene la información de una cuenta por su numero móvil",							"CONSULTA A TRX 390" ,"FEI_026"),
	CONSULTA_CUENTA_DOLARES(			"CONSULTA_CUENTA_DOLARES",				"Obtener LZA0 Obtener PE47",				"/transacciones/rest/cuentas_dolares",				"Obtiene la información del LZA0",													"CONSULTA A TRX 390" ,"FEI_027"),
	CONSULTA_BUC_POR_RFC(				"CONSULTA_BUC_POR_RFC",					"Obtener PE47",								"/transacciones/rest/rfcs",							"Obtiene la información del PE47",													"CONSULTA A TRX 390" ,"FEI_028"),
	CONSULTA_COLONIAS(					"CONSULTA_COLONIAS",					"Obtener LT23",								"/transacciones/rest/codigos_postales",				"Obtiene la información del LT23",													"CONSULTA A TRX 390" ,"FEI_030"),
	REGISTRA_PISTAS_DE_AUDITORIA(		"REGISTRA_PISTAS_DE_AUDITORIA",			"Registrar Pistas de Auditoría",			"/pistasformatos/pistas",							"Registra las pistas de auditoría de las operaciones generadas por el negocio",		"REGISTRO EN BASE DE DATOS" ,"FEI_031"),
	CONSULTA_COOKIE(					"CONSULTA_COOKIE",						"Obtener Cookie",							"/sessions/cookie",									"Obtiene o actualiza la información de la cookie aplicativa",						"CONSULTA A BASE DE DATOS" ,"FEI_032"),
	GENERA_CIERRA_SESION(				"GENERA_CIERRA_SESION",					"--",										"/ResolverWS/ResolverServices/ResolverServices.wsdl","Es el servicio que se invoca para generar y terminar las sesiones del negocio",	"REGISTRO EN BASE DE DATOS" ,"FEI_033"),
	CONSULTA_ESTADO_SESION(				"CONSULTA_ESTADO_SESION",				"Consular Sesión",							"/session/consultar",								"Consulta el estado de la sesión",													"CONSULTA A BASE DE DATOS" ,"FEI_034"),
	ACTUALIZA_SESION(					"ACTUALIZA_SESION",						"Actualizar Sesión",						"/session/actualizar",								"Actualiza la sesión actual",														"ACTUALIZACION EN BASE DE DATOS" ,"FEI_035"),
	REGENERA_COOKIE_APLICATIVA(			"REGENERA_COOKIE_APLICATIVA",			"Regenerar Cookie Aplicativa",				"/session/regenerar_cookie",						"Vuelve a generar la cookie aplicativa",											"REGISTRO EN BASE DE DATOS" ,"FEI_037"),
	VALIDA_PERFIL(						"VALIDA_PERFIL",						"Validar Perfilamiento de URL",				"/perfilamiento/validar",							"Valida el perfilamiento de seguridad de los servicios por su url",					"CONSULTA A BASE DE DATOS" ,"FEI_038"),;
	
	private String code;
	private String name;
	private String url;
	private String detail;
	private String type;
	private String id;

	private Transacciones(String code, String name, String url, String detail, String type, String id) {
		this.code = code;
		this.name = name;
		this.url = url;
		this.detail = detail;
		this.type = type;
		this.id = id;
	}
	
	public static Transacciones getTransacciones(String code) {
		for (Transacciones elemento : Transacciones.values()) {
			if (elemento.getCode().equals(code)) {
				return elemento;
			}
		}
		return Transacciones.DEFAULT;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public String getDetail() {
		return detail;
	}

	public String getType() {
		return type;
	}

	public String getId() {
		return id;
	}
}
