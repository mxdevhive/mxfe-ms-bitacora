package mx.isban.fe.autenticacion.service;

import mx.isban.bboo.commons.exception.BusinessException;
import mx.isban.bboo.commons.messages.EstatusOperaciones;
import mx.isban.fe.bitacora.dto.AutenticationRequest;
import mx.isban.fe.bitacora.enums.Transacciones;

public interface IAutenticacionService {

	/**
	 * 
	 * Registra el resultado de la operacion de la autenticacion
	 * 
	 *
	 * 
	 * @param usuario
	 * 
	 *            usuario que intenta ingresar al aplicativo
	 * 
	 * @param ipOrigen
	 * 
	 *            ip de donde se realiza la solicitud
	 * 
	 * @param idSesion
	 * 
	 *            identificador de la sesion
	 * 
	 * @param instanciaWeb
	 * 
	 *            instancia web desde donde se ejecuta
	 * 
	 * @param servidorWeb
	 * 
	 *            servidor web desde donde se ejecuta
	 * 
	 * @param estatus
	 * 
	 *            estatus de la operacion
	 * 
	 * @param transaccion
	 * 
	 *            transaccion a registrar
	 */
	void registarPistaAuditoria(AutenticationRequest request) throws BusinessException;

}
