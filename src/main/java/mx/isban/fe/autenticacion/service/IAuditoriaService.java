/**
 * ISBAN México - (c) Banco Santander Central Hispano
 *
 * Creado el 12/12/2017 18:41:46 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.service;

import mx.isban.fe.autenticacion.model.PistaAuditoriaBean;

/**
 * 
 * @Proyecto: bo-autenticacion-service-dev
 * @Paquete : mx.isban.bboo.autenticacion.service
 * @Clase   : IAuditoriaService.java.
 * <br/>
 * 
 * @version 1.0.0
 */
public interface IAuditoriaService {
    /**
     * Registra la pista de auditoria del servicio
     * 
     * @param pista datos a registrar en la pista de auditoria
     */
    void registrarPistaAuditoria(PistaAuditoriaBean pista);
}
