/**
 * ISBAN México - (c) Banco Santander Central Hispano
 *
 * Creado el 12/12/2017 18:42:06 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.service;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import mx.isban.bboo.commons.model.ResponseCode;
import mx.isban.bboo.commons.utilerias.ZeroJsonXml;
import mx.isban.fe.autenticacion.model.PistaAuditoriaBean;
import mx.isban.fe.autenticacion.model.ResponseAuditoriaBean;
import mx.isban.fe.bitacora.config.MxfeMsBitacoraConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestClientException;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * @Proyecto: bo-autenticacion-service-dev
 * @Paquete : mx.isban.bboo.autenticacion.service
 * @Clase : AuditoriaService.java. <br/>
 *        Clase que implementa la funcionalidad de pistas de auditoria. Debera
 *        cumplir con el manejo de errores con esta capa
 * @version 1.0.0
 */
@Service
public class AuditoriaService implements IAuditoriaService {

	/**
	 * Logging de la aplicacion
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AuditoriaService.class);
	/**
	 * Template para consumo del servicio de pistas de auditoria
	 */
	@Autowired
	private AsyncRestTemplate restTemplate;
	/**
	 * Permite obtener la configuracion de la url solicitada
	 */
	@Autowired
	private MxfeMsBitacoraConfig restConfiguration;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.isban.bboo.autenticacion.service.IAuditoriaService#registrarPistaAuditoria
	 * (mx.isban.bboo.autenticacion.model.PistaAuditoriaBean)
	 */
	@Override
	@HystrixCommand(fallbackMethod = "registrarPistaAuditoriaFallback")
	public void registrarPistaAuditoria(PistaAuditoriaBean pista) {
		//String transaccion = ZeroDateTime.transaccion();
		try {
			//            LOGGER.info("[{}-{}] Ingresando --> AuditoriaService --> registrarPistaAuditoria", transaccion,
			//                    ZeroDateTime.now());
			LOGGER.info("Parametros para registrar la pista de auditoria. {}", pista.toString());
			// Se obtiene la url para el servicio de pistas de auditoia
			String urlPistas = restConfiguration.getUrlPistas();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<PistaAuditoriaBean> request = new HttpEntity<>(pista, headers);
			// Se invoca el servicio de pistas de auditoria
			ListenableFuture<ResponseEntity<String>> futureResponse = restTemplate.exchange(urlPistas, HttpMethod.POST, request, String.class);
			ResponseEntity<String> response = futureResponse.get();
			//            		restTemplate.exchange(urlPistas, HttpMethod.POST, request, String.class);
			LOGGER.info("Resultado de pistas --> {}", response);

			// Se obtiene la respuesta del servicio
			ResponseAuditoriaBean responseBean = ZeroJsonXml.toObject(new ResponseAuditoriaBean(), response.getBody());
			// Se evalua respuesta del servicio
			if (!responseBean.isOk()) {
				LOGGER.error("Error al registrar la pista de auditoria.");
				if (responseBean.getValidations().isEmpty()) {
					LOGGER.error("Error de negocio.");
					LOGGER.error("Validacion --> {}", responseBean.getResponseCode().getMessage());
				} else {
					LOGGER.error("Validaciones de negocio.");
				}
				// Se obtienen validaciones
				for (ResponseCode validation : responseBean.getValidations()) {
					LOGGER.error("Validacion --> {}", validation.getMessage());
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error al procesar el response del registro de la pista de auditoria", e);
		} catch (RestClientException e) {
			LOGGER.error("Error en el consumo del servicio registrarPistaAuditoria", e);
		} catch (InterruptedException e) {
			LOGGER.error("Error en el consumo del servicio registrarPistaAuditoria", e);
		} catch (ExecutionException e) {
			LOGGER.error("Error en el consumo del servicio registrarPistaAuditoria", e);
		}
		//        LOGGER.info("[{}-{}] Saliendo --> AuditoriaService --> registrarPistaAuditoria", transaccion,
		//                ZeroDateTime.now());
	}

	/**
	 * Registra la pista de auditoria del servicio
	 * 
	 * @param pista
	 *            datos a registrar en la pista de auditoria
	 */
	public void registrarPistaAuditoriaFallback(PistaAuditoriaBean pista) {
		// no hace nada en fallback
		LOGGER.warn("Ingresando AuditoriaService --> registrarPistaAuditoriaFallback");
	}

}
