package mx.isban.fe.autenticacion.service;

import mx.isban.bboo.commons.exception.BusinessException;
import mx.isban.fe.autenticacion.model.PistaAuditoriaBean;
import mx.isban.fe.bitacora.dto.AutenticationRequest;
import mx.isban.fe.bitacora.enums.Transacciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AutenticacionService implements IAutenticacionService {
	
	@Autowired
	private IAuditoriaService auditoriaService;
	
	private static final String ID_APP = "INTRAMX-APPEB-SSO_FORMATOSELECT";
	private static final String MS_REPORTES = "mxfe-ms-reportes";
	private static final String MS_TRANSACCIONES = "mxfe-ms-transacciones";

	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * 
	 * 
	 * @see mx.isban.bboo.autenticacion.service.IAutenticacionService#
	 * 
	 * registarPistaAuditoria(java.lang.String, java.lang.String,
	 * java.lang.String,
	 * 
	 * java.lang.String, java.lang.String,
	 * 
	 * mx.isban.bboo.commons.messages.EstatusOperaciones)
	 */

	@Override
	@Async("threadPoolTaskExecutor")
	public void registarPistaAuditoria(AutenticationRequest request)  throws BusinessException {

		// Generar pistas de auditoria
		Transacciones transaccion = Transacciones.getTransacciones(request.getCode());

		String detalleOperacion = transaccion.getDetail();
		String tipoCodOperacion = transaccion.getType();
		
		if( request.getWebService().equalsIgnoreCase(MS_REPORTES) || (request.getWebService().equalsIgnoreCase(MS_TRANSACCIONES) && !request.getStatusOper().equals("OK"))){
				
			detalleOperacion = request.getMsgResponse();
		}
		
		PistaAuditoriaBean pista = new PistaAuditoriaBean();
		pista.setAplicativo(ID_APP);
		pista.setIpOrigen(request.getRemoteAddr());
		pista.setResultadoOperacion(request.getStatusOper().equals("OK") ? "Operación realizada exitosamente." : "Operación erronea.");
		pista.setTipoOperacion(tipoCodOperacion);
		pista.setCodigoOperacion(tipoCodOperacion);//name
		pista.setUsuario(request.getUser());
		pista.setOperacion(transaccion.getCode());
		pista.setDetalleOperacion(detalleOperacion);
		//"Generacion de sesion para la autenticacion del usuario, se verifica el token corporativo y se registra la sesion en redis.");
		pista.setInstanciaWeb(request.getWebInstance());
		pista.setServidorWeb(request.getWebService());
		pista.setIdSesion(request.getSessionId());
		pista.setIdOperacion(transaccion.getId());
		pista.setEstatusOperacion(request.getStatusOper() == null 
				|| !request.getStatusOper().trim().toUpperCase().equals("OK") ? "Cancelada" : "Aceptada");
		pista.setNombreTransaccion(transaccion.getUrl());
		pista.setReferencia(request.getReference());
		pista.setNombreArchivo(request.getFileName());

		auditoriaService.registrarPistaAuditoria(pista);

	}

}
