/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 *
 * Creado el 25/11/2017 23:39:59 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @Proyecto: bo-auditoria-service
 * @Paquete: mx.isban.bboo.auditoria.model
 * @Clase: PistaAuditoriaBean.java. <br/>
 *         Bean para pistas de auditoria
 * @version 1.0.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PistaAuditoriaBaseBean implements Serializable {

    /**
     * Identificador de serializacion
     */
    private static final long serialVersionUID = -180258185010356857L;

    /**
     * Estatus de la operacion a registrar
     */
    protected String estatusOperacion;
    /**
     * identificador de la operacion a registrar
     */
    protected String idOperacion;
    
    /**
     * Nombre de la transaccion que se llevo a cabo
     */
    protected String nombreTransaccion;

    /**
     * Constructor de la clase
     */
    public PistaAuditoriaBaseBean() {
        super();
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de estatusOperacion.
     */
    public String getEstatusOperacion() {
        return estatusOperacion;
    }

    /**
     * Fija el valor.
     *
     * @param estatusOperacion
     *            el valor de estatusOperacion a colocar
     */
    public void setEstatusOperacion(String estatusOperacion) {
        this.estatusOperacion = estatusOperacion;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de idOperacion.
     */
    public String getIdOperacion() {
        return idOperacion;
    }

    /**
     * Fija el valor.
     *
     * @param idOperacion
     *            el valor de idOperacion a colocar
     */
    public void setIdOperacion(String idOperacion) {
        this.idOperacion = idOperacion;
    }
    
    /**
     * Obtener valor.
     * @return El valor de nombreTransaccion.  
     */
    public String getNombreTransaccion() {
        return nombreTransaccion;
    }

    /**
     * Fija el valor.
     *
     * @param nombreTransaccion el valor de nombreTransaccion a colocar
     */
    public void setNombreTransaccion(String nombreTransaccion) {
        this.nombreTransaccion = nombreTransaccion;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PistaAuditoriaBaseBean [estatusOperacion=" + estatusOperacion + ", idOperacion=" + idOperacion
                + ", nombreTransaccion=" + nombreTransaccion + "]";
    }

}
