/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 *
 * Creado el 25/11/2017 23:39:59 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @Proyecto: bo-auditoria-service
 * @Paquete: mx.isban.bboo.auditoria.model
 * @Clase: PistaAuditoriaBean.java. <br/>
 *         Bean para pistas de auditoria
 * @version 1.0.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PistaAuditoriaBean extends PistaAuditoriaBaseBean implements Serializable {

    /**
     * Identificador de serializacion
     */
    private static final long serialVersionUID = -180258185010356857L;

    private String ipOrigen;
    private String aplicativo;
    private String usuario;
    private String tipoOperacion;
    private String codigoOperacion;
    private String resultadoOperacion;
    private String detalleOperacion;
    private String operacion;
    private String idSesion;
    private String instanciaWeb;
    private String servidorWeb;
    private String generaReporte;
    private String referencia;
    private String nombreArchivo;

    /**
     * Constructor de la clase
     */
    public PistaAuditoriaBean() {
        super();
    }

    /**
     * Modelo para la peticion del registro de una pista de auditoria
     * 
     * @param ipOrigen
     *            ip de donde se genera la operacion
     * @param aplicativo
     *            aplicativo de donde se genera la operacion
     * @param usuario
     *            usuario quien genera la operacion
     * @param tipoOperacion
     *            tipo de operacion que se realiza
     * @param codigoOperacion
     *            codigo de operacion
     * @param resultadoOperacion
     *            resultado de la operacion que se realizo
     * @param detalleOperacion
     *            detalle de la operacion que se realizo
     * @param idSesion
     *            identificador de la sesion
     * @param instanciaWeb
     *            instancia web
     */
    public PistaAuditoriaBean(String ipOrigen, String aplicativo, String usuario, String tipoOperacion,
            String codigoOperacion, String resultadoOperacion, String operacion, String detalleOperacion,
            String idSesion,
            String instanciaWeb) {
        super();
        this.ipOrigen = ipOrigen;
        this.aplicativo = aplicativo;
        this.usuario = usuario;
        this.tipoOperacion = tipoOperacion;
        this.resultadoOperacion = resultadoOperacion;
        this.detalleOperacion = detalleOperacion;
        this.codigoOperacion = codigoOperacion;
        this.idSesion = idSesion;
        this.instanciaWeb = instanciaWeb;
    }

    /**
     * @return the ipOrigen
     */
    public String getIpOrigen() {
        return ipOrigen;
    }

    /**
     * @param ipOrigen
     *            the ipOrigen to set
     */
    public void setIpOrigen(String ipOrigen) {
        this.ipOrigen = ipOrigen;
    }

    /**
     * @return the aplicativo
     */
    public String getAplicativo() {
        return aplicativo;
    }

    /**
     * @param aplicativo
     *            the aplicativo to set
     */
    public void setAplicativo(String aplicativo) {
        this.aplicativo = aplicativo;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario
     *            the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the tipoOperacion
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * @param tipoOperacion
     *            the tipoOperacion to set
     */
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    /**
     * @return the resultadoOperacion
     */
    public String getResultadoOperacion() {
        return resultadoOperacion;
    }

    /**
     * @param resultadoOperacion
     *            the resultadoOperacion to set
     */
    public void setResultadoOperacion(String resultadoOperacion) {
        this.resultadoOperacion = resultadoOperacion;
    }

    /**
     * @return the detalleOperacion
     */
    public String getDetalleOperacion() {
    	if(detalleOperacion == null)
    		detalleOperacion = "Eformats Operation";
    	if(detalleOperacion.length() > 4000){
    		detalleOperacion = detalleOperacion.substring(0,4000);
    	}
        return detalleOperacion;
    }

    /**
     * @param detalleOperacion
     *            the detalleOperacion to set
     */
    public void setDetalleOperacion(String detalleOperacion) {
        this.detalleOperacion = detalleOperacion;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de codigoOperacion.
     */
    public String getCodigoOperacion() {
    	if(codigoOperacion == null)
    		codigoOperacion = "Eformats Oper";
        return codigoOperacion;
    }

    /**
     * Fija el valor.
     *
     * @param codigoOperacion
     *            el valor de codigoOperacion a colocar
     */
    public void setCodigoOperacion(String codigoOperacion) {
        this.codigoOperacion = codigoOperacion;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de idSesion.
     */
    public String getIdSesion() {
    	if(idSesion.length() > 150){
    		idSesion = idSesion.substring(0, 150);
    	}
        return idSesion;
    }

    /**
     * Fija el valor.
     *
     * @param idSesion
     *            el valor de idSesion a colocar
     */
    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de instanciaWeb.
     */
    public String getInstanciaWeb() {
        return instanciaWeb;
    }

    /**
     * Fija el valor.
     *
     * @param instanciaWeb
     *            el valor de instanciaWeb a colocar
     */
    public void setInstanciaWeb(String instanciaWeb) {
        this.instanciaWeb = instanciaWeb;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de servidorWeb.
     */
    public String getServidorWeb() {
        return servidorWeb;
    }

    /**
     * Fija el valor.
     *
     * @param servidorWeb
     *            el valor de servidorWeb a colocar
     */
    public void setServidorWeb(String servidorWeb) {
        this.servidorWeb = servidorWeb;
    }

    public String getGeneraReporte(){
    	if(generaReporte == null){
    		generaReporte = "true";
    	}
    	return generaReporte;
    }
    
    public void setGeneraReporte(String generaReporte){
    	this.generaReporte = generaReporte;
    }
    

	public String getOperacion() {
		if(operacion == null)
			operacion = "Eformats Oper";
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	@Override
	public String toString() {
		return "PistaAuditoriaBean [ipOrigen=" + ipOrigen + ", aplicativo="
				+ aplicativo + ", usuario=" + usuario + ", tipoOperacion="
				+ tipoOperacion + ", codigoOperacion=" + codigoOperacion
				+ ", resultadoOperacion=" + resultadoOperacion
				+ ", detalleOperacion=" + detalleOperacion + ", operacion="
				+ operacion + ", idSesion=" + idSesion + ", instanciaWeb="
				+ instanciaWeb + ", servidorWeb=" + servidorWeb
				+ ", generaReporte=" + generaReporte + ", referencia="
				+ referencia + ", nombreArchivo=" + nombreArchivo + "]";
	}

}
