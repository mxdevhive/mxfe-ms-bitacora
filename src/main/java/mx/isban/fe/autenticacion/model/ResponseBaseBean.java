/**
 * ISBAN México - (c) Banco Santander Central Hispano
 *
 * Creado el 12/12/2017 17:39:43 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import mx.isban.bboo.commons.model.ResponseCode;

/**
 * 
 * @Proyecto: bo-autenticacion-service-dev
 * @Paquete : mx.isban.bboo.autenticacion.model
 * @Clase : ResponseBaseBean.java. <br/>
 * 
 * @version 1.0.0
 */
@JsonIgnoreProperties(ignoreUnknown = false)
public class ResponseBaseBean implements Serializable {

    /**
     * Serializador
     */
    private static final long serialVersionUID = -8314200481488601542L;

    private ResponseCode responseCode;
    private boolean ok;
    private List<ResponseCode> validations;

    /**
     * Constructor de la clase
     */
    public ResponseBaseBean() {
        super();
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de ok.
     */
    public boolean isOk() {
        return ok;
    }

    /**
     * Fija el valor.
     *
     * @param ok
     *            el valor de ok a colocar
     */
    public void setOk(boolean ok) {
        this.ok = ok;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de responseCode.
     */
    public ResponseCode getResponseCode() {
        return responseCode;
    }

    /**
     * Fija el valor.
     *
     * @param responseCode
     *            el valor de responseCode a colocar
     */
    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de validations.
     */
    public List<ResponseCode> getValidations() {
        if (validations == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(validations);
    }

    /**
     * Fija el valor.
     *
     * @param validations
     *            el valor de validations a colocar
     */
    public void setValidations(List<ResponseCode> validations) {
        if (validations == null) {
            this.validations = new ArrayList<>();
        } else {
            this.validations = new ArrayList<>(validations);
        }

    }

}
