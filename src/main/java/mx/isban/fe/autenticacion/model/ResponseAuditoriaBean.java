/**
 * ISBAN México - (c) Banco Santander Central Hispano
 *
 * Creado el 12/12/2017 17:39:43 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.fe.autenticacion.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @Proyecto: bo-autenticacion-service-dev
 * @Paquete : mx.isban.bboo.autenticacion.model
 * @Clase   : ResponseAuditoriaBean.java.
 * <br/>
 * 
 * @version 1.0.0
 */
@JsonIgnoreProperties(ignoreUnknown = false)
public class ResponseAuditoriaBean extends ResponseBaseBean implements Serializable {

    /**
     * Serializador
     */
    private static final long serialVersionUID = -8314200481488601542L;

    private PistaAuditoriaBean response;

    /**
     * Constructor de la clase
     */
    public ResponseAuditoriaBean() {
        super();
    }

    /**
     * Obtener valor.
     * 
     * @return El valor de response.
     */
    public PistaAuditoriaBean getResponse() {
        return response;
    }

    /**
     * Fija el valor.
     *
     * @param response
     *            el valor de response a colocar
     */
    public void setResponse(PistaAuditoriaBean response) {
        this.response = response;
    }

}
